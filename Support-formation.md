# Introduction kubernetes

## Qu'est ce que kubernetes ?

https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/

## De quoi est il composé ?

https://kubernetes.io/docs/concepts/overview/components/

## Comment discute-t-on avec lui ?

## Les objets kubernetes

Node

Namespace

Pod

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: static-web
  labels:
    role: myrole
    app: nginx-api
    version: v1.0
  annotations:

spec:
    containers:
        -
            name: web
            image: nginx:master
            ports:
                -
                    name: web
                    containerPort: 80
                    protocol: TCP
            envFrom: {name: "app-config", key: "mysql-password"}
            env:
                - { name: "VERSION", value: "v1.0" }
                - { name: "COMMIT_SHA", value: "4321750893260" }
                - { name: "MYSQL_PASS", valueFrom: {  secretKeyRef { name: "app-config", key: "mysql-password" } }}
            ressources:
                limit:
                    cpu: "500m"
                    memory: "50Mi"
                request:
                    cpu: "50m"
                    memory: "50Mi"
            livenessProbe:
                httpGet:
                    path: /
                    port: 80
                initialDelaySeconds: 5
                periodSeconds: 5
                failureThreshold: 1
            readynessProbe:
                httpGet:
                    path: /
                    port: 80
            volumeMounts:
                -
                    name: assets
                    mountPath: /var/www/assets
                -
                    name: log-config
                    mountPath: /var/log
        -
            name: front
            containerPort: 9000
            volumeMounts:
                -
                    name: assets
                    mountPath: /var/www/assets
    volumes:
        -
            name: assets
            emptyDir: {}
        -
            name: log-config
            configMap:
                name: log-config
                items:
                    -
                        key: log_level
                        path: log_level

```

ReplicaSet
Deployment

DeamonSet
StatefulSet
HorizontalPodAutoscaler
PodDisruptionBudget

Job
CronJob

Services
ClusterIP
NodePort
LoadBalancer
ExternalName
ExternalIp

Ingress

```yaml
# Source: ep-api/templates/nginx-ingress.yaml
apiVersion: "networking.k8s.io/v1beta1"
kind: "Ingress"
metadata:
  name: "api-nginx"
  namespace: "espace-api"
  annotations:
    cert-manager.io/acme-challenge-type: http01
    cert-manager.io/cluster-issuer: letsencrypt
    kubernetes.io/ingress.class: "nginx"
    nginx.ingress.kubernetes.io/configuration-snippet: |
      real_ip_header X-Forwarded-For;
      set_real_ip_from 0.0.0.0/0;
spec:
  tls:
    - hosts:
        - "api.monapi.com"
      secretName: "api.monapi.com-tls"
  rules:
    - host: "api.monapi.com"
      http:
        paths:
          - path: "/"
            backend:
              serviceName: "api-nginx"
              servicePort: 80
    - host: "api.monapi.com"
      http:
        paths:
          - path: "/swagger"
            backend:
              serviceName: "api-swagger-nginx"
              servicePort: 8080
```

PersistentVolumeClaim
PersistentVolume

ConfigMap

```yaml
kind: ConfigMap
apiVersion: v1
metadata:
  name: app-config
  namespace: default
data:
  mysql-password: "rewqirpewiuq[o"
```

Secret

```yaml
kind: Secret
apiVersion: v1
metadata:
  name: app-config
  namespace: default
data:
  mysql-password: "rewqirpewiuq[o"
```

CustomRessources

## Exercice 1

## Ingress

https://miro.medium.com/max/2400/1*KIVa4hUVZxg-8Ncabo8pdg.png
