## 1. Qu'est ce que kubernetes ? (cocher une ou plusieurs cases)
- [ ] C'est un orchestrateur de conteneurs.
- [ ] Portabilité d’infrastructure
- [ ] C'est un service Saas
- [ ] Docker registry
- [ ] Veut dire Capitaine

## 2. Parmis cette liste, cocher les types de services existants ? (cocher une ou plusieurs cases)
- [ ] NodePort
- [ ] ClusterIP
- [ ] ExternalName
- [ ] ExternalRessources
- [ ] LoadBalancer

## 3. Dans quel process est stocké l'état du cluster ? (cocher une ou plusieurs cases)
- [ ] kube-proxy
- [ ] kubelet
- [ ] etcd
- [ ] api-server
- [ ] controller-manager

## 4. Quel est l'objet qui permet de gérer l'accès au cluster par DNS ?
- [ ] Service
- [ ] Ingress
- [ ] Node
- [ ] Deployments
- [ ] DaemonSet

## 5. Est ce que l'objet PersistentVolume est namespacé ?
- [ ] oui
- [ ] non

## 6. Comparaison entre Deployment et Statefulset (cocher ce qui est vrai, plusieurs réponses possibles)
- [ ] Deployment est adapté aux applications stateless et StatefulSet est adapté aux applications stateful
- [ ] Deployment n'est pas compatible avec les PV alors que StatefulSet oui
- [ ] Deployment ont des UID aléatoires, StatefulSet à des identifiants uniques
- [ ] Deployment et StatefulSet peuvent avoir pusieurs replicas

## 7. Que peut contenir un pod ? (cocher ce qui est vrai, plusieurs réponses possibles)
- [ ] Des conteneurs
- [ ] Des Volumes
- [ ] Des Services
- [ ] Des Labels
- [ ] Une définition de périodicité d'exécution (schedule)
