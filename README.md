Mettre en place votre env gcp :

- installer `git`: `sudo apt install git`
- clone du repository : `git clone https://gitlab.com/henres/kubernetes-formation.git`
- install kubectl: https://kubernetes.io/docs/tasks/tools/#kubectl
- install helm: https://helm.sh/docs/intro/install/
- install microk8s :
  - Linux:
  https://microk8s.io/docs/getting-started
  - Windows:
  https://microk8s.io/docs/install-windows
  - Macos:
  https://microk8s.io/docs/install-macos
- start microk8s:
  - start microk8s `microk8s start`
  - save kubectl config `microk8s config > ~/.kube/config` (create .kube folder if needed)
  - check if the cluster answering `kubectl cluster-info`
- enable addon:
  - `microk8s enable metallb:192.168.1.240/24`
  - `microk8s enable ingress`
  - `microk8s enable dashboard`


Obtention de l'auto-completion:
* `apt-get install bash-completion `
* `kubectl completion bash | sudo tee /etc/bash_completion.d/kubectl > /dev/null`
* `source ~/.bashrc`
